# Introduction

This repository contains my experiments writing a deserializer of `key=value` type strings into structures, using template metaprogramming. The code can derive deserializers for structs containing base types and enums (as long as you implement the `Default` and `FromStr` traits for them). The example `main.rs` program deserializes the following string:

```
num=42
txt=Hello
choice=OneDoesNotMatch
choice2=Two

```

Into the following structure:

```Rust
enum TestEnum {
    Def,
    One,
    Two
}

struct Test {
    num: u32,
    num2: u8,
    txt: String,
    txt2: String,
    choice: TestEnum,
    choice2: TestEnum,
}
```

Struct fields not included in the string or having invalid values (e.g. `choice=OneDoesNotMatch`) are filled with default values. Result of the execution of this program is:

```
Test {
    num: 42,
    num2: 0,
    txt: "Hello",
    txt2: "",
    choice: Def,
    choice2: Two,
}
```

