// Here we only define the trait that kv-extract-derive module will automatically implement.
pub trait KvExtract {
    fn kv_extract(input: &str) -> Self;
}
